'use strict';

angular.module('clientApp').config(function($routeProvider) {
  $routeProvider
    .when('/', {
      controller: 'HomeCtrl',
      templateUrl: 'views/home.html'
    })
    .when('/chat', {
      controller: 'ChatCtrl',
      templateUrl: 'views/chat.html',
      resolve: {

      }
    })
    .otherwise('/');
});
