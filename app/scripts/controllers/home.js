'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('HomeCtrl', function ($scope, $location, $auth, toastr, $http, envConfig) {
    $scope.logout = function() {
      var token = $auth.getToken();
      console.log('Logout do token...', token);
      $http.delete(envConfig.logoutUrl + token).then(
        function() {
          $auth.logout().then(
            function() {
              toastr.info('Logout realizado.');
              $location.path('/');
            }
          );
        }, function(response) {
          toastr.info('Não foi possivel realizar o logout.');
          console.log('Unable to logout.', response);
        }
      );
    };

    $scope.authenticate = function(provider) {
      console.log('Authenticate with ' + provider);
      $auth.authenticate(provider)
        .then(function(response) {
          console.log('Signed in with Google.', response);
          $location.path('/chat');
        })
        .catch(function(response) {
          console.log('Something went wrong.', response);
        });
    };

    $scope.joinChat = function() {
      console.log('Joining chat with: ' + $auth.getToken());
      $location.path('/chat');
    };

    $scope.isAuthenticated = function() {
      return $auth.isAuthenticated();
    };
  });
