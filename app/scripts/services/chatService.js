'use strict';

angular.module('clientApp').factory('chatAPI', function($websocket, $location, envConfig) {
  var _ws = null;

  var _monitoraMensagens = function(client) {
    _ws = $websocket(envConfig.chatWebSocketUrl);

    _ws.onOpen(function() {
      _ws.send(client.handshake);
    });

    _ws.onMessage(function(message) {
      console.log('Msg recebida', message);
      client.read(JSON.parse(message.data));
    });
    console.log('Monitoramento de mensagens iniciado..');
  };

  var _enviaMensagem = function(msg) {
    _ws.send(msg);
    console.log('Msg enviada.', msg);
  };

  var _cancelaMonitoramentoMensagens = function() {
    _ws.close();
    console.log('Monitoramento de mensagens encerrado.');
  };

  return {
    monitoraMensagens: _monitoraMensagens,
    enviaMensagem: _enviaMensagem,
    cancelaMonitoramentoMensagens: _cancelaMonitoramentoMensagens
  };
});
