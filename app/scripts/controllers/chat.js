'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:ChatCtrl
 * @description
 * # ChatCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('ChatCtrl', function ($scope, $location, $auth, chatAPI, $window, $timeout) {

    $scope.viewers = [];
    var blurTimeout;

    chatAPI.monitoraMensagens({
      handshake: {
        token: $auth.getToken()
      },
      read: function(message) {
        switch (message.type) {
          case 'USER_INFO':
                $scope.user = message.userInfo;
                $scope.messageLog = 'Bem vindo, ' + message.userInfo.name + '!';
                break;
          case 'OTHER_SUBSCRIBERS':
                $scope.viewers = message.people;
                break;
          case 'SUBSCRIBE':
                $scope.viewers.push(message.userInfo);
                $scope.messageLog += '\n' + message.userInfo.name + ' entrou no chat.';
                break;
          case 'TEXT':
                $scope.messageLog += '\n' + message.userInfo.name + ': ' + message.text;
                break;
          case 'LEAVE':
                $scope.viewers = $scope.viewers.filter(function(el) {
                  return el.email !== message.userInfo.email;
                });
                $scope.messageLog += '\n' + message.userInfo.name + ' saiu do chat.';
                break;
        }
      }
    });

    $scope.leaveChat = function() {
      $location.path('/home');
    };

    $scope.sendMessage = function(message) {
      if (!message) {
        return;
      }
      chatAPI.enviaMensagem({
        text: message,
        token: $auth.getToken()
      });
      $scope.message = '';
    };

    $scope.$on('$destroy', function() {
      chatAPI.cancelaMonitoramentoMensagens();
    });

    $window.onfocus = function() {
      if (!blurTimeout) {
        return;
      }

      $timeout.cancel(blurTimeout);
    };

    $window.onblur = function() {
      blurTimeout = $timeout(function() {
        $scope.sendMessage('Desconectado por inatividade.');
        $scope.leaveChat();
      }, 30 * 1000);
    };
  }
);
