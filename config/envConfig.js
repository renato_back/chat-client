'use strict';

angular.module('services.config', [])
  .constant('envConfig', {
    chatWebSocketUrl: '@@chatWebSocketUrl',
    baseUrl: '@@baseUrl',
    logoutUrl: '@@logoutUrl'
  });
