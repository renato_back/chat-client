# client

Projeto do cliente para o Chat da Pixeon. - [Live demo](http://pixeon-chat-client.herokuapp.com/)

## Build & development

Este projeto utiliza o Node e seu Node Package Manager (NPM), então é necessário instalá-lo antes.
Mais detalhes na [página do Node](http://nodejs.org).

Após instalar o NPM, execute o comando `npm install`.

Para rodar o serviço de client, o projeto utiliza o Grunt, então é preciso instalá-lo globalmente para poder iniciar o serviço. 
Para isso, execute `npm install -g grunt`. 

O projeto depende do Bower para gerenciar suas dependências de javascript. Para instalá-lo, execute `npm install -g bower`.
Após instalar o bower, execute `bower install` para baixar as bibliotecas necessárias.

Executando `grunt`, você fará o build do projeto. E com `grunt serve` ele subirá um serviço e abrirá uma página no navegador para acessar o chat.

**Obs.:** Para utilizar o serviço, é preciso que o módulo __server__ esteja rodando na mesma máquina. Detalhes [aqui](https://bitbucket.org/renato_back/chat-server).

## Testing

Executar `grunt test` fará com que os testes sejam realizados, porém no momento o projeto não possui testes efetivos.


## Deploying

Presumindo que já foi feito o commit de todas as alterações:

`grunt production`

`grunt build`

`cd dist`

`git add -A`

`git commit -m "Latest changes`

`cd ..`

`grunt deploy`
