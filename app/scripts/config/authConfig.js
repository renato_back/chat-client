'use strict';

angular.module('clientApp').config(function($authProvider, envConfig) {
  $authProvider.google({
    clientId: '564415615014-jdql8f1tadv80e2bvv8igfodmgu3g4gr.apps.googleusercontent.com'

  });

  $authProvider.baseUrl = envConfig.baseUrl;
});
